<?php

namespace Drupal\shopify_app\Plugin\ShopifyWebhook;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\shopify_app\ShopInterface;
use Drupal\shopify_app\WebhookPluginBase;
use Shopify\Webhooks\Topics;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The app uninstalled webhook plugin.
 *
 * @ShopifyWebhook(
 *   id = "shopify_app_uninstalled"
 * )
 */
class AppUninstalled extends WebhookPluginBase {

  /**
   * Shop entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $shopStorage;

  /**
   * Constructs a plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->shopStorage = $entityTypeManager->getStorage('shopify_app_shop');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function topics(): array {
    return [
      Topics::APP_UNINSTALLED,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function handle(string $topic, string $shop, array $requestBody): void {
    $shop_entity = $this->shopStorage->load($shop);
    if ($shop_entity instanceof ShopInterface) {
      $this->shopStorage->delete([$shop_entity]);
    }
  }

}
