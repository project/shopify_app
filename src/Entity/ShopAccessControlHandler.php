<?php

namespace Drupal\shopify_app\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the shop entity type.
 */
class ShopAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'delete' || $operation == 'update') {
      return AccessResult::allowedIfHasPermission($account, 'administer shopify_app shop configuration');
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
