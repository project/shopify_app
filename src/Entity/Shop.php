<?php

namespace Drupal\shopify_app\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\shopify_app\ShopInterface;
use Drupal\user\UserInterface;
use Shopify\Auth\Session;

/**
 * Defines the shop entity.
 *
 * @ConfigEntityType(
 *   id = "shopify_app_shop",
 *   label = @Translation("Shop"),
 *   label_collection = @Translation("Shops"),
 *   label_singular = @Translation("shop"),
 *   label_plural = @Translation("shops"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shops",
 *     plural = "@count shops",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\shopify_app\Entity\ShopAccessControlHandler",
 *     "list_builder" = "Drupal\shopify_app\Entity\ShopListBuilder",
 *     "form" = {
 *       "add" = "Drupal\shopify_app\Form\ShopEditForm",
 *       "edit" = "Drupal\shopify_app\Form\ShopEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "shop",
 *   admin_permission = "administer shopify_app shop configuration",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "delete-form" =
 *   "/admin/structure/shopify-app-shop/manage/{shopify_app_shop}/delete",
 *     "edit-form" =
 *   "/admin/structure/shopify-app-shop/manage/{shopify_app_shop}",
 *     "collection" = "/admin/structure/shopify-app-shop"
 *   },
 *   config_export = {
 *     "id",
 *     "impersonate_user_id"
 *   }
 * )
 */
class Shop extends ConfigEntityBase implements ShopInterface {

  /**
   * The form ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The user id of which requests from shopify shoud be impersonated as.
   *
   * @var int
   */
  protected $impersonate_user_id;

  /**
   * {@inheritDoc}
   */
  public function label() {
    return $this->id();
  }

  /**
   * {@inheritDoc}
   */
  public function getImpersonateUser(): ?UserInterface {
    if (empty($this->impersonate_user_id)) {
      return NULL;
    }
    /**
     * @var \Drupal\user\UserInterface|null $user
     */
    $user = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($this->impersonate_user_id);

    return $user;
  }

  /**
   * {@inheritDoc}
   */
  public function setImpersonateUser(UserInterface $user = NULL): void {
    $this->impersonate_user_id = $user !== NULL ? $user->id() : NULL;
  }

  /**
   * Get the shop entity related to the given session.
   *
   * @param \Shopify\Auth\Session $session
   *   The session.
   *
   * @return \Drupal\shopify_app\ShopInterface
   *   The shop entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function getForSession(Session $session): ShopInterface {
    $storage = \Drupal::entityTypeManager()->getStorage('shopify_app_shop');

    $shop = $storage->load($session->getShop());

    if (!$shop instanceof ShopInterface) {
      $shop = $storage->create([
        'id' => $session->getShop(),
      ]);
      $shop->save();
    }

    if ($shop instanceof ShopInterface) {
      return $shop;
    }

    throw new \RuntimeException('Failed retrieving shop entity');
  }

}
