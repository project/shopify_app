<?php

namespace Drupal\shopify_app;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a contact form entity.
 */
interface ShopInterface extends ConfigEntityInterface {

  /**
   * Get the user requests from Shopify are impersonated as.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user.
   */
  public function getImpersonateUser(): ?UserInterface;

  /**
   * Set the user requests from Shopify are impersonated as.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   The user
   */
  public function setImpersonateUser(UserInterface $user = NULL): void;

}
