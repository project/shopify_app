<?php

namespace Drupal\shopify_app;

use Shopify\Webhooks\Handler;

/**
 * The webhook handler.
 */
class WebhookHandler implements Handler {

  /**
   * Webhook plugin manager.
   *
   * @var \Drupal\shopify_app\WebhookPluginManager
   */
  protected $webhookPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\shopify_app\WebhookPluginManager $webhookPluginManager
   *   The webhook plugin manager.
   */
  public function __construct(WebhookPluginManager $webhookPluginManager) {
    $this->webhookPluginManager = $webhookPluginManager;
  }

  /**
   * Handle a webhook.
   *
   * @param string $topic
   *   The topic.
   * @param string $shop
   *   The shop.
   * @param array $body
   *   The request body.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function handle(string $topic, string $shop, array $body): void {
    foreach ($this->webhookPluginManager->getDefinitions() as $definition) {
      /**
       * @var \Drupal\shopify_app\WebhookPluginInterface $plugin
       */
      $plugin = $this->webhookPluginManager->createInstance($definition['id']);

      if (!in_array($topic, $plugin->topics())) {
        continue;
      }

      try {
        $plugin->handle($topic, $shop, $body);
      }
      catch (\Exception $e) {
        watchdog_exception('shopify_app', $e);
      }
    }
  }

}
