<?php

namespace Drupal\shopify_app\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base form for shop edit forms.
 *
 * @internal
 */
class ShopEditForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\shopify_app\ShopInterface $shop
     */
    $shop = $this->entity;

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shop id'),
      '#default_value' => $shop->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\shopify_app\Entity\Shop::load',
      ],
      '#disabled' => !$shop->isNew(),
    ];

    $impersonate_user = $shop->getImpersonateUser();

    $form['impersonate_user_id'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('Impersonate requests from shopify as'),
      '#default_value' => $impersonate_user,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\shopify_app\ShopInterface $shop
     */
    $shop = $this->entity;
    $impersonate_user_id = $form_state->getValue('impersonate_user_id');
    if (!empty($impersonate_user_id)) {
      $impersonate_user = $this->entityTypeManager->getStorage('user')
        ->load($impersonate_user_id);
    }
    else {
      $impersonate_user = NULL;
    }
    $shop->setImpersonateUser($impersonate_user);
    $status = $shop->save();

    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')
      ->toString();
    $label = $shop->label();
    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addStatus($this->t('Shop %label has been updated.', ['%label' => $label]));
      $this->logger('contact')
        ->notice('Shop %label has been updated.', [
          '%label' => $shop->label(),
          'link' => $edit_link,
        ]);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Shop %label has been added.', ['%label' => $label]));
      $this->logger('contact')
        ->notice('Shop %label has been added.', [
          '%label' => $shop->label(),
          'link' => $edit_link,
        ]);
    }

    $form_state->setRedirectUrl($shop->toUrl('collection'));

    return $status;
  }

}
