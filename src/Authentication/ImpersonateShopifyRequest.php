<?php

namespace Drupal\shopify_app\Authentication;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\UserSession;
use Drupal\shopify_app\Access\AppAccessCheck;
use Drupal\shopify_app\ShopInterface;
use Shopify\Auth\Session;
use Symfony\Component\HttpFoundation\Request;

/**
 * Impersonate requests from shopify.
 */
class ImpersonateShopifyRequest implements AuthenticationProviderInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * App shop entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $shopifyAppShopStorage;

  /**
   * Constructor.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entityTypeManager) {
    $this->connection = $connection;
    $this->shopifyAppShopStorage = $entityTypeManager->getStorage('shopify_app_shop');
  }

  /**
   * {@inheritDoc}
   */
  public function applies(Request $request) {
    try {
      $session = AppAccessCheck::getSession($request);
      $shop = $this->shopifyAppShopStorage->load($session->getShop());
      if (!$shop instanceof ShopInterface) {
        return FALSE;
      }
      if ($shop->getImpersonateUser() === NULL) {
        return FALSE;
      }

      return $session instanceof Session;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function authenticate(Request $request) {
    $session = AppAccessCheck::getSession($request);
    /**
     * @var \Drupal\shopify_app\ShopInterface $shop
     */
    $shop = $this->shopifyAppShopStorage->load($session->getShop());

    $values = $this->connection
      ->query('SELECT * FROM {users_field_data} [u] WHERE [u].[uid] = :uid AND [u].[default_langcode] = 1', [
        ':uid' => $shop->getImpersonateUser()
          ->id(),
      ])
      ->fetchAssoc();

    // Check if the user data was found and the user is active.
    if (!empty($values) && $values['status'] == 1) {
      // Add the user's roles.
      $rids = $this->connection
        ->query('SELECT [roles_target_id] FROM {user__roles} WHERE [entity_id] = :uid', [':uid' => $values['uid']])
        ->fetchCol();
      $values['roles'] = array_merge([AccountInterface::AUTHENTICATED_ROLE], $rids);

      return new UserSession($values);
    }

    return NULL;
  }

}
