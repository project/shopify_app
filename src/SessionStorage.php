<?php

namespace Drupal\shopify_app;

use Drupal\Core\Database\Connection;
use Shopify\Auth\Session;
use Shopify\Auth\SessionStorage as ShopifySessionStorage;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Shopify Session storage.
 */
class SessionStorage implements ShopifySessionStorage {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $db
   *   Database connection.
   */
  public function __construct(Connection $db) {
    $this->db = $db;
  }

  /**
   * {@inheritDoc}
   */
  public function storeSession(Session $session): bool {
    $this->deleteSession($session->getId());
    $this->db->insert('shopify_app_session')->fields([
      'session_id' => $session->getId(),
      'data' => serialize($session),
      'shop' => $session->getShop(),
      'is_online' => $session->isOnline() ? 1 : 0,
    ])->execute();
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function loadSession(string $sessionId) {
    $data = $this->db->select('shopify_app_session', 's')
      ->fields('s', ['session_id', 'data'])
      ->condition('session_id', $sessionId)
      ->execute()
      ->fetchAssoc();

    if (empty($data) || !isset($data['data'])) {
      return NULL;
    }

    $session = unserialize($data['data'], [
      'allowed_classes' => [
        Session::class,
        \stdClass::class,
      ],
    ]);

    if (!$session instanceof Session) {
      $this->deleteSession($sessionId);
      return NULL;
    }

    if ($session->getExpires() !== NULL) {
      if ($session->getExpires()->getTimestamp() <= time()) {
        // Session expired.
        $this->deleteSession($sessionId);
        return NULL;
      }
    }

    return $session;
  }

  /**
   * {@inheritDoc}
   */
  public function deleteSession(string $sessionId): bool {
    $this->db->delete('shopify_app_session')
      ->condition('session_id', $sessionId)
      ->execute();
    return TRUE;
  }

  /**
   * Get the offline session for the given shop.
   *
   * @param string $shop
   *   The shop.
   *
   * @return \Shopify\Auth\Session|null
   *   The offline session.
   */
  public function getOfflineSession(string $shop): ?Session {
    static $cache = [];

    if (isset($cache[$shop])) {
      return $cache[$shop];
    }

    $data = $this->db->select('shopify_app_session', 's')
      ->fields('s', ['session_id'])
      ->condition('shop', $shop)
      ->condition('is_online', '0')
      ->execute()
      ->fetchAssoc();

    if (empty($data) || !isset($data['session_id'])) {
      return NULL;
    }

    $cache[$shop] = $this->loadSession($data['session_id']);

    return $cache[$shop];
  }

  /**
   * Load the current shopify session.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param string|null $shop
   *   The shop.
   *
   * @return \Shopify\Auth\Session|null
   *   The shopify session
   */
  public function getCurrentSession(SessionInterface $session, string $shop = NULL): ?Session {
    if ($shop === NULL && $session->has('shopify_app_session_shop')) {
      $shop = $session->get('shopify_app_session_shop');
    }
    if (empty($shop)) {
      throw new \InvalidArgumentException('Shop is missing');
    }
    $shopify_session_id = $session->get('shopify_app_session_id:' . $shop);
    if (!is_string($shopify_session_id)) {
      return NULL;
    }
    return $this->loadSession($shopify_session_id);
  }

  /**
   * Set the current shopify session.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param \Shopify\Auth\Session $shopifySession
   *   The shopify session.
   */
  public function setCurrentSession(SessionInterface $session, Session $shopifySession): void {
    if (!$session->isStarted()) {
      $session->migrate();
    }
    $session->set('shopify_app_session_id:' . $shopifySession->getShop(), $shopifySession->getId());
    $session->set('shopify_app_session_shop', $shopifySession->getShop());
    $session->save();
  }

  /**
   * Create a new instance.
   *
   * @return \Drupal\shopify_app\SessionStorage
   *   The new instance.
   */
  public static function getInstance(): SessionStorage {
    static $instance = NULL;
    if (!$instance instanceof SessionStorage) {
      $instance = new self(\Drupal::database());
    }
    return $instance;
  }

}
