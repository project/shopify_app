<?php

namespace Drupal\shopify_app;

/**
 * Interface for webhook plugins.
 */
interface WebhookPluginInterface {

  /**
   * Get a list of topics this plugin can handle.
   *
   * @return string[]
   *   The topics.
   */
  public function topics(): array;

  /**
   * Handle the topic.
   *
   * @param string $topic
   *   The topic.
   * @param string $shop
   *   The shop.
   * @param array $requestBody
   *   The request body.
   */
  public function handle(string $topic, string $shop, array $requestBody): void;

}
