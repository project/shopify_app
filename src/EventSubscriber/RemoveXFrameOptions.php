<?php

namespace Drupal\shopify_app\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Remove the x-frame-options header to allow embedding in shopify admin.
 */
class RemoveXFrameOptions implements EventSubscriberInterface {

  /**
   * Remove the header.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function removeXframeOptionsHeader(ResponseEvent $event) {
    $event->getResponse()->headers->remove('X-Frame-Options');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['removeXframeOptionsHeader', -10];
    return $events;
  }

}
