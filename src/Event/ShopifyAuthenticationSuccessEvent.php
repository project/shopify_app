<?php

namespace Drupal\shopify_app\Event;

use Shopify\Auth\Session;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Fired when shopify autentication succeeded.
 */
class ShopifyAuthenticationSuccessEvent extends Event {

  /**
   * The Shopify session.
   *
   * @var \Shopify\Auth\Session
   */
  public $session;

  /**
   * Constructor.
   *
   * @param \Shopify\Auth\Session $session
   *   The Shopify session.
   */
  public function __construct(Session $session) {
    $this->session = $session;
  }

}
