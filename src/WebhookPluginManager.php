<?php

namespace Drupal\shopify_app;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\shopify_app\Annotation\ShopifyWebhook;

/**
 * The webhook plugin manager.
 */
class WebhookPluginManager extends DefaultPluginManager {

  /**
   * WebhookPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    $this->subdir = 'Plugin/ShopifyWebhook';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = ShopifyWebhook::class;
    $this->pluginInterface = WebhookPluginInterface::class;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Get all topics plugins are listening for.
   *
   * @return string[]
   *   The topics
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getAllTopics(): array {
    $topics = [];

    foreach ($this->getDefinitions() as $definition) {
      /**
       * @var \Drupal\shopify_app\WebhookPluginInterface $plugin
       */
      $plugin = $this->createInstance($definition['id']);

      $topics = array_merge($topics, $plugin->topics());
    }

    return $topics;
  }

}
