<?php

namespace Drupal\shopify_app\Controller;

use Drupal\Core\Controller\ControllerBase;
use Shopify\Webhooks\Registry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * The webhook controller.
 */
class WebhookController extends ControllerBase {

  /**
   * Handle a webhook.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response
   */
  public function handle(Request $request):Response {
    shopify_app_initialize_context();

    try {
      $response = Registry::process($request->headers->all(), $request->getContent());

      if (!$response->isSuccess()) {
        throw new \RuntimeException($response->getErrorMessage());
      }

      return new Response('OK');
    }
    catch (\Exception $e) {
      watchdog_exception('shopify_app', $e);
      throw $e;
    }
  }

}
