<?php

namespace Drupal\shopify_app\Controller;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\shopify_app\Event\ShopifyAuthenticationSuccessEvent;
use Drupal\shopify_app\SessionStorage;
use Drupal\shopify_app\WebhookPluginManager;
use Shopify\Auth\OAuth;
use Shopify\Auth\OAuthCookie;
use Shopify\Auth\Session;
use Shopify\Webhooks\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Manage Shopify app authentication.
 */
class AuthController extends ControllerBase {

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Webhook plugin manager.
   *
   * @var \Drupal\shopify_app\WebhookPluginManager
   */
  protected $webhookPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\shopify_app\WebhookPluginManager $webhookPluginManager
   *   The webhook plugin manager.
   */
  public function __construct(ContainerAwareEventDispatcher $eventDispatcher, WebhookPluginManager $webhookPluginManager) {
    $this->eventDispatcher = $eventDispatcher;
    $this->webhookPluginManager = $webhookPluginManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('plugin.manager.shopify_app.webhook')
    );
  }

  /**
   * Authenticate route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array|\Drupal\Core\Routing\TrustedRedirectResponse
   *   The response.
   */
  public function authenticate(Request $request) {
    $shop = $request->get('shop');

    if (empty($shop)) {
      throw new NotFoundHttpException();
    }

    shopify_app_initialize_context();

    $return_url = Url::fromRoute('shopify_app.oauth_callback')
      ->setAbsolute(FALSE)
      ->toString(TRUE)->getGeneratedUrl();

    $response = new TrustedRedirectResponse('/');

    $redirect_url = OAuth::begin($shop, $return_url, FALSE, function (OAuthCookie $cookie) use ($response) {
      // Cookie expiration is 1 minute by default.
      $expire = $cookie->getExpire() + 3600;
      $response_cookie = new Cookie(
        $cookie->getName(),
        $cookie->getValue(),
        $expire,
        '/',
        NULL,
        $cookie->isSecure(),
        $cookie->isHttpOnly(),
        FALSE,
        'none'
      );
      $response->headers->setCookie($response_cookie);

      return TRUE;
    });
    $response->setTrustedTargetUrl($redirect_url);
    $response->setCache([
      'max_age' => 0,
      'private' => TRUE,
      'must_revalidate' => TRUE,
      'no_cache' => TRUE,
      'no_store' => TRUE,
    ]);

    return $response;
  }

  /**
   * Oauth callback route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   */
  public function oauthCallback(Request $request) {
    $config = $this->config('shopify_app.settings');
    $session_storage = SessionStorage::getInstance();

    shopify_app_initialize_context();

    $response = new TrustedRedirectResponse('/');

    $shopify_session = OAuth::callback(
      $request->cookies->all(),
      $request->query->all(),
      function (OAuthCookie $cookie) use ($response) {
        $response_cookie = new Cookie(
          $cookie->getName(),
          $cookie->getValue(),
          $cookie->getExpire(),
          '/',
          NULL,
          $cookie->isSecure(),
          $cookie->isHttpOnly()
        );
        $response->headers->setCookie($response_cookie);

        return TRUE;
      });

    $session = $request->getSession();
    $session_storage->setCurrentSession($session, $shopify_session);
    $session->set('shopify_app_auth_shop', $shopify_session->getShop());

    $this->registerWebhooks($shopify_session);

    $this->eventDispatcher->dispatch(new ShopifyAuthenticationSuccessEvent($shopify_session));

    $frontpage_route = $config->get('frontpage_route');
    if (empty($frontpage_route)) {
      $frontpage_route = 'shopify_app.frontpage';
    }

    $url = Url::fromRoute($frontpage_route);

    $response = new RedirectResponse($url->setAbsolute()
      ->toString(TRUE)
      ->getGeneratedUrl());
    $response->setCache([
      'max_age' => 0,
      'private' => TRUE,
    ]);
    return $response;
  }

  /**
   * Register listening for webhooks.
   *
   * @param \Shopify\Auth\Session $session
   *   The session.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   * @throws \Shopify\Exception\InvalidArgumentException
   * @throws \Shopify\Exception\UninitializedContextException
   * @throws \Shopify\Exception\WebhookRegistrationException
   */
  protected function registerWebhooks(Session $session): void {
    foreach ($this->webhookPluginManager->getAllTopics() as $topic) {
      $response = Registry::register(
        Url::fromRoute('shopify_app.webhook')
          ->toString(TRUE)
          ->getGeneratedUrl(),
        $topic,
        $session->getShop(),
        $session->getAccessToken(),
      );

      if (!$response->isSuccess()) {
        throw new \RuntimeException('Could not register webhook for topic ' . $topic . ': ' . json_encode($response));
      }
    }
  }

}
