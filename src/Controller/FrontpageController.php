<?php

namespace Drupal\shopify_app\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Shopify app frontpage controller.
 */
class FrontpageController extends ControllerBase {

  /**
   * Build a simple front page.
   *
   * @return array
   *   The render array.
   */
  public function build():array {
    return [
      '#type' => 'markup',
      '#markup' => $this->config('system.site')->get('name'),
    ];
  }

}
