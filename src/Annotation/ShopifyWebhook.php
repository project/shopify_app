<?php

namespace Drupal\shopify_app\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Shopify webhook annotation.
 *
 * @Annotation
 */
class ShopifyWebhook extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
