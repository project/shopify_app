<?php

namespace Drupal\shopify_app\Access;

use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\shopify_app\SessionStorage;
use Shopify\Auth\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * The access check for shopify app routes.
 */
class AppAccessCheck implements AccessInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * Get the current session.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Shopify\Auth\Session|null
   *   The session
   */
  public static function getSession(Request $request): ?Session {
    $session_storage = SessionStorage::getInstance();
    $shopify_session_id = $request->cookies->get('shopify_session_id');

    if ($shopify_session_id !== NULL) {
      // Current session.
      return $session_storage->loadSession($shopify_session_id);
    }
    else {
      // Possible app proxy request.
      $valid_shop_domain = self::validateRequestShopDomain($request);
      $valid_signature = self::validateRequestSignature($request);
      if (!$valid_shop_domain || !$valid_signature) {
        $request_session = $request->getSession();
        if ($request_session !== NULL) {
          // Not an app proxy request, possible a current session already exists?
          $current_session = $session_storage->getCurrentSession($request->getSession());

          if ($current_session instanceof Session) {
            return $current_session;
          }
        }

        throw new AccessDeniedHttpException('Not a shopify request');
      }

      return $session_storage->getOfflineSession($request->get('shop'));
    }
  }

  /**
   * Determine if we have access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param \Drupal\Core\Routing\RouteMatch $routeMatch
   *   The current route match.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   The result.
   */
  public function access(AccountInterface $account, RouteMatch $routeMatch) {
    shopify_app_initialize_context();

    $request = $this->requestStack->getCurrentRequest();

    $shopify_session = self::getSession($request);

    return AccessResult::allowedIf($shopify_session instanceof Session);
  }

  /**
   * Validate the requests shop domain.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return bool
   *   Valid or not.
   */
  public static function validateRequestShopDomain(Request $request): bool {
    $shop_domain = $request->get('shop');
    if (empty($shop_domain)) {
      return FALSE;
    }
    $substring = explode('.', $shop_domain);

    // 'blah.myshopify.com'
    if (count($substring) != 3) {
      return FALSE;
    }

    // Allow dashes and alphanumberic characters.
    $substring[0] = str_replace('-', '', $substring[0]);
    return (ctype_alnum($substring[0]) && $substring[1] . '.' . $substring[2] == 'myshopify.com');
  }

  /**
   * Validate the request signature.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return bool
   *   valid or not
   */
  public static function validateRequestSignature(Request $request): bool {
    $params = $request->query->all();
    if (!isset($params['signature'])) {
      return FALSE;
    }
    $signature = $params['signature'];
    unset($params['signature']);
    ksort($params);
    $params_array = [];
    foreach ($params as $key => $value) {
      $params_array[] = "{$key}={$value}";
    }

    $sig = implode("", $params_array);
    $calculatedSig = hash_hmac('sha256', $sig, \Drupal::config('shopify_app.settings')
      ->get('app_secret'));

    return hash_equals($signature, $calculatedSig);
  }

}
