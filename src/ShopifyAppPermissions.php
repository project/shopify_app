<?php

namespace Drupal\shopify_app;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class ShopifyAppPermissions {

  use StringTranslationTrait;

  public function permissions() {
    $permissions = [];

    $shops = \Drupal::entityTypeManager()
      ->getStorage('shopify_app_shop')
      ->loadMultiple();

    foreach ($shops as $shop) {
      $permissions['administer shopify shop ' . $shop->id()] = [
        'title' => $this->t('Administer Shopify shop @shop', ['@shop' => $shop->label()]),
        'restrict access' => TRUE,
      ];
    }

    return $permissions;
  }

}
