<?php

namespace Drupal\shopify_app\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\shopify_app\Access\AppAccessCheck;
use Drupal\shopify_app\Authentication\ImpersonateShopifyRequest;
use Shopify\Auth\Session;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cache policy for pages served for shopify.
 */
class RequestPolicy implements RequestPolicyInterface {

  /**
   * The authentication provider.
   *
   * @var \Drupal\shopify_app\Authentication\ImpersonateShopifyRequest
   */
  protected $impersonateShopifyRequest;

  /**
   * constructor.
   *
   * @param \Drupal\shopify_app\Authentication\ImpersonateShopifyRequest $impersonateShopifyRequest
   *   The authentication provider
   */
  public function __construct(ImpersonateShopifyRequest $impersonateShopifyRequest) {
    $this->impersonateShopifyRequest = $impersonateShopifyRequest;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {

    if ($this->impersonateShopifyRequest->applies($request)) {
      return self::DENY;
    }

    try {
      if (AppAccessCheck::getSession($request) instanceof Session) {
        return self::DENY;
      }
    }
    catch (\Exception $e) {
      // Ignore this.
    }

    return NULL;
  }

}
